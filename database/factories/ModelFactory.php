<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\User::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->email,
        'password' => bcrypt(str_random(10)),
        'remember_token' => str_random(10),
    ];
});


$factory->define(App\Models\Coordinator::class, function (Faker\Generator $faker) {
    return [
        'firstname' => $faker->firstName,
        'middlename' => $faker->lastName,
        'lastname' => $faker->lastName,
        'gender' => $faker->randomElement(['Male','Female']),
        'position' => $faker->randomElement(['Team Leader', 'Asst Team Leader', 'Member']),
        'email' => $faker->email,
        'password' => bcrypt(str_random(10)),
    ];
});


$factory->define(App\Models\Report::class, function (Faker\Generator $faker) {
    return [
        'barangay' => $faker->city,
        'coordinatorId' => $faker->randomElement([1, 4, 5, 6]),
        'reportDate' => $faker->dateTime,
        'streetAddress' => $faker->streetAddress,
        'activityRoute' => $faker->paragraph,
        'feedback' => $faker->paragraph,
        'task_id' => $faker->randomElement(['1', '2', '3']),
        'photo' => $faker->url,
        'caption' => $faker->text,
        'longitude'=> $faker->longitude,
        'latitude' => $faker->latitude
    ];
});


