<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReport extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reports', function (Blueprint $table) {
            $table->increments('id');
            $table->dateTime('reportDate');
            $table->integer('coordinatorId');
            $table->string('barangay');
            $table->string('streetAddress');
            $table->integer('task_id');
            $table->string('activityRoute');
            $table->string('feedback');
            $table->string('photo');
            $table->string('caption');
            $table->string('latitude');
            $table->string('longitude');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('reports');
    }
}
