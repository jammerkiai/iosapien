<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBarangays extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('barangays', function (Blueprint $table) {
            $table->increments('id');
            $table->string('RegionCode');
            $table->string('ProvinceCode', 20);
            $table->string('MunicipalityCode', 20);
            $table->string('BarangayCode', 20);
            $table->string('BarangayName');
            $table->string('BarangayLatitude');
            $table->string('BarangayLongitude');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('barangays');
    }
}
