# DashBase

Dashboard base application built on top of Laravel's default Auth scaffolding.

It is meant to serve as a springboard for developing new web apps that require
a minimum of standard ACL defaults.


** Installation: **

1. Clonet this repository or download the zip file onto your dev machine
2. Run composer install
3. Copy .env.example to .env
4. Run `php artisan key:generate` creates a hash key and updates .env file with it
5. Create a mysql database named dashbase 
6. Edit .env file and update the DB_ configurations to match your setup
7. Run `chmod -R 777 storage/*` to allow the application to create or modify log files
8. Run `chmod -R 777 bootstrap/cache` to allow the application to create or modify cache files
9. Run `php artisan migrate` to create and initialize the user tables used by the auth module
10. For LOCAL development: Run `php artisan serve` to quickly test by going to http://localhost:8000 on the browser
11. For server installation, addtional notes:
    APACHE: 
      - make sure mod_rewrite is enabled
      - in apache.conf Directory directive, make sure "AllowOverride All" 
        instead of "AllowOverride None" 



** Requires: **

- PHP 5.3^ 
- Laravel 5.2
- MySQL