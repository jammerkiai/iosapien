<?php

namespace App\Traits;

use Illuminate\Http\Request;
use Log;
use App\Models\Report;


trait HandleReports {

    public function saveReport(Request $request) {
        $destinationPath = storage_path() . '/app/uploads/';

        $photo = $request->file('photo');
        Log::info($destinationPath);
        $input = $request->except(['photo', '_url']);
        Log::info($request);
        $input['photo'] = $input['coordinatorId'] . '_' . $photo->getClientOriginalName();
        $input['reportDate'] = date('Y-m-d H:i:s');


        Report::create($input);

        $photo->move($destinationPath, $input['photo']);

        Log::info($input);
    }

}
