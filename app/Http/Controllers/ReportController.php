<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Report;
use DB;
use App\Traits\HandleReports;

class ReportController extends Controller
{
    use HandleReports;

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index() {
        $reports = Report::paginate(30);

        return view('reports.index', compact('reports'));
    }

    public function show($id) {

        $report = Report::findOrFail($id);
        return view('reports.show', compact('report'));
    }

    public function create() {
        return view('reports.create');
    }

    public function store(Request $request) {
        $this->saveReport($request);
        return redirect('reports')->with('message', 'Report Saved');
    }

}
