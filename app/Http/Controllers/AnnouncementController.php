<?php

namespace App\Http\Controllers;

use App\Models\Announcement;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class AnnouncementController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index() {
        $announcements = Announcement::paginate('50');

        return view('announcements.index', compact('announcements'));
    }

    public function create() {
        return view('announcements.create');
    }

    public function store(Request $request) {
        $inputs = $request->except(['_url', '_token']);

        if ($request->has('id')) {
            Announcement::find($inputs['id'])->update($inputs);
            $msg = 'Announcement updated';
        } else {
            Announcement::create($inputs)->save();
            $msg = 'New announcement saved';
        }

        return redirect('announcements')->with('message', $msg);
    }

    public function edit($id) {
        $announcement = Announcement::find($id);
        return view('announcements.edit', compact('announcement'));
    }

    public function destroy($id) {
        Announcement::destroy($id);
        return redirect('announcements')->with('message', 'Item deleted');
    }
 }
