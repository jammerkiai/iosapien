<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Contact;


class ContactController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index() {
        $contacts = Contact::paginate(20);

        return view('contacts.index', compact('contacts'));
    }

    public function show($id) {
        $contact = Contact::findOrFail($id);
        return view('contacts.index', compact('contact'));
    }
}
