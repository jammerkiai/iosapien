<?php

namespace App\Http\Controllers;

use App\Models\Locator;
use App\Models\Municipality;
use App\Models\Report;
use App\Models\Message;
use App\Models\Meeting;
use App\Models\Announcement;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Coordinator;
use App\Models\Task;
use App\Models\Contact;
use DB;
use Log;
use App\Traits\HandleReports;

class ExternalController extends Controller
{
    use HandleReports;

    public function creds(Request $request) {

        $input = $request->all();

        $co = Coordinator::where('email', $input['user'])
            ->where('password',$input['pass'])
            ->first();

        $sql = "select m.*, r.RegionName, p.ProvinceName
                from regions r, provinces p, municipalities m, coordinator_municipalities cm
                where m.RegionCode = r.RegionCode
                and m.ProvinceCode = p.ProvinceCode
                and m.MunicipalityCode = cm.MunicipalityCode
                and cm.id = ?
                order by r.RegionCode, p.ProvinceCode, m.MunicipalityCode";

        if (is_null($co)) {
            return response()->json(['success' => 'false']);
        }

        $muni = DB::select($sql, [$co->toArray()['id']]);

        $task = Task::all();

        $resp = [
            'user' => $co->toArray(),
            'areas' => $muni,
            'tasks' => $task->toArray()
        ];

        return response()->json($resp, 200);
    }

    public function report(Request $request) {
        $this->saveReport($request);
        return response()->json(['success' => true]);
    }

    public function contact(Request $request) {
        $input = $request->all();

        if ($input['firstname'] == '' or $input['lastname'] == '') {
            return response()->json(['success' => false]);
        }

        Contact::create($input);
        Log::info($input);
        return response()->json(['success' => true]);
    }

    public function locator(Request $request) {
        $input = $request->except(['_url']);

        $loc = Locator::where('coordinatorId', [$input['coordinatorId']])->get();

        if ( count($loc) == 0 ) {
            Locator::create($input)->save();
        } else {
            Locator::where('coordinatorId', [$input['coordinatorId']])->update($input);
        }

        return response()->json(['success' => true]);
    }

    public function getLocators() {
        $loc = DB::select('select c.id, concat(c.firstname, " " , c.lastname) as coordinatorName,
            c.mobile, c.position,
            l.longitude, l.latitude
            from coordinators c, locator l where c.id=l.coordinatorId');


        return response()->json($loc);
    }

    public function announcements() {
        $an = Announcement::orderBy('id', 'desc')->get();
        return response()->json($an);
    }

    public function coordinators() {
        $cors = Coordinator::all();

        $ret = [];
        foreach ($cors as $cor) {
	    if (!isset($cor->municipalities()[0])) continue;
            $mun = Municipality::where("MunicipalityCode", [$cor->municipalities()[0]->MunicipalityCode])->first();
            $tmp = [
                "id" => $cor->id,
                "firstname" => $cor->firstname,
                "lastname" => $cor->lastname,
                "gender" => $cor->gender,
                "mobile" => $cor->mobile,
                "municipality" => $mun->MunicipalityName,
                "province" => $mun->province->ProvinceName
            ];
            array_push($ret, $tmp);
        }
        return response()->json($ret);
    }

    public function getmessages() {
        $msgs = Message::orderBy('id', 'desc')->get();
        return response()->json($msgs);
    }

    public function postmessage(Request $request) {
        $input = $request->except(['_url', '_token']);
        $msg = Message::create($input);
        return response()->json(['success' => true, 'data' => $msg]);
    }

    public function getmessage($id) {
        $msgs = Message::find($id);

        if ($msgs) {
            return response()->json($msgs);
        }

        return response()->json([]);
    }

    public function getmeetings() {
        $msgs = Meeting::orderBy('id', 'desc')->get();
        return response()->json($msgs);
    }

    public function postmeeting(Request $request) {
        $input = $request->except(['_url', '_token']);
        $msg = Meeting::create($input);
        return response()->json(['success' => true, 'data' => $msg]);
    }

    public function getmeeting($id) {
        $msgs = Meeting::find($id);

        if ($msgs) {
            return response()->json($msgs);
        }

        return response()->json([]);
    }

    public function confirmMeeting(Request $request) {
        $inputs = $request->except(['_url', '_token']);

        if ($request->has('id')) {
            Meeting::find($inputs['id'])->update($inputs);
            $msg = 'Meeting updated';
            return response()->json(['success' => true, 'message' => $msg]);
        }

        return response()->json(['success' => false, 'message' => 'Error with request']);

    }
}
