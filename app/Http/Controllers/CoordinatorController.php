<?php

namespace App\Http\Controllers;

use App\Models\CoordinatorMunicipality;
use App\Models\Municipality;
use App\Models\Region;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Coordinator;
use Session;
use DB;
use Validator;


class CoordinatorController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }



    public function index() {

        $coordinators = Coordinator::paginate(20);
        return view('coordinators.index', compact('coordinators'));
    }

    public function create() {
        return view('coordinators.create');
    }

    public function edit($id) {

        $coordinator = Coordinator::findOrFail($id);
        return view('coordinators.edit', compact('coordinator'));

    }

    public function areas($id) {
        $coordinator = Coordinator::findOrFail($id);
        $regions = Region::all();

        $cms = CoordinatorMunicipality::where('id', $id)->select('MunicipalityCode')->get();
        $cities = [];
        foreach($cms as $cm) {
            array_push($cities, $cm->MunicipalityCode);
        }
        return view('coordinators.areas', compact('coordinator', 'regions', 'cities'));
    }


    public function saveAreas(Request $request) {
        $input = $request->all();

        if ($request->has('ProvinceCode')) {

            $cities = Municipality::where('ProvinceCode', $input['ProvinceCode'])->get();

            foreach ($cities as $city) {
                $res = $this->toggleCoordinatorMunicipality($input['id'], $city->MunicipalityCode, $input['checked']);
            }

        } else {

            $res = $this->toggleCoordinatorMunicipality(
                $input['id'], $input['MunicipalityCode'], $input['checked']
            );

        }

        return response()->json($res);

    }

    protected function toggleCoordinatorMunicipality($coordinator, $municipality, $checked) {


        DB::delete('delete from coordinator_municipalities where id=? and MunicipalityCode=?',
            [ $coordinator, $municipality ] );

        $msg = 'City removed';
        $act = 'remove';

        if ($checked === 'true' or $checked === true) {

            DB::insert('insert into coordinator_municipalities(id, MunicipalityCode) values(?,?)',
                [ $coordinator, $municipality ] );

            $msg = 'City Added';
            $act = 'add';
        }

        return ['action' => $act, 'message' => $msg];
    }


    public function update($id, Request $request) {

        $coordinator = Coordinator::findOrFail($id);

        $segment = $request->get('segment');

        if ($segment == 'auth') {

            $input = $request->all();



            $validator = Validator::make($input, [
                'email' => 'required|email',
                'password' => 'required',
                'password_confirmation' => 'required|same:password'
            ]);


            if ($validator->fails()) {
                return redirect()->back()->withErrors($validator);
            }

            $coordinator->password = $input['password'];
            $coordinator->save();

            $msg = 'Coordinator credentials successfully updated!';


        } elseif ($segment == 'personal') {

            $this->validate($request, [
                'firstname' => 'required',
                'middlename' => 'required',
                'lastname' => 'required',
                'birthday' => 'required',
                'mobile' => 'required',
                'gender' => 'required',
                'position' => 'required',
                'active' => 'required',
            ]);

            $input = $request->all();

            $coordinator->fill($input)->save();
            $msg = 'Coordinator successfully updated!';
        }



        Session::flash('flash_message', $msg);

        return redirect()->back();

    }

    public function store(Request $request) {

        $this->validate($request,[
            'firstname' => 'required',
            'middlename' => 'required',
            'lastname' => 'required',
            'email' => 'required|email',
            'password' => 'required',
            'birthday' => 'required',
            'mobile' => 'required',
            'gender' => 'required',
            'position' => 'required',
            'active' => 'required',
        ]);

        $input = $request->all();
        $input['password'] = $input['password'];
        Coordinator::create($input);

        Session::flash('flash_message', 'coordinator successfully added!');

        return redirect()->back();
    }

    public function destroy($id) {
        $coordinator = Coordinator::findOrFail($id);
        $coordinator->delete();
        Session::flash('flash_message', 'coordinator successfully deleted!');

        return redirect()->route('coordinators.index');
    }
}
