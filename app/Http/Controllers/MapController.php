<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Coordinator;
use App\Models\Report;
use DB;


class MapController extends Controller
{
    public function coordinators() {

        $sql = "select c.id, ws_concat(' ', c.firstname, c.lastname) as name,
                c.gender, c.position,
                from coordinators c, municipalites m
                where c.active=1
                AND ";
        $coordinators = DB::select($sql);

        return response()->json($coordinators);
    }

    public function reports() {
        $sql = "select r.id, r.reportDate, r.barangay, r.streetAddress,
              r.photo, r.caption, r.longitude, r.latitude,
              concat(c.firstname, ' ', c.lastname) as coordinator,
              t.name as task
              from  reports r, coordinators c, tasks t
              where r.coordinatorId=c.id
              and r.task_id=t.id";
        $reports = DB::select($sql);
        return response()->json(['reports' => $reports]);
    }

    public function tasks() {

    }
}
