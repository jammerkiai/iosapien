<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

Route::group(['prefix' => 'api', 'middleware' => 'cors'], function () {
    Route::post('creds', 'ExternalController@creds');
    Route::post('report', 'ExternalController@report');
    Route::post('contact', 'ExternalController@contact');
    Route::post('locator', 'ExternalController@locator');
    Route::get('announcements', 'ExternalController@announcements');
    Route::get('coordinators', 'ExternalController@coordinators');

    Route::group(['prefix' => 'map'], function () {
       Route::get('coordinators', 'MapController@coordinators');
       Route::get('reports', 'MapController@reports');
       Route::get('tasks', 'MapController@tasks');
       Route::get('locator', 'ExternalController@getLocators');
    });

    Route::group(['prefix' => 'tiger'], function () {
        Route::get('message', 'ExternalController@getmessages');
        Route::get('message/{id}', 'ExternalController@getmessage');
        Route::post('message', 'ExternalController@postmessage');

        Route::get('meeting', 'ExternalController@getmeetings');
        Route::get('meeting/{id}', 'ExternalController@getmeeting');
        Route::post('meeting', 'ExternalController@postmeeting');
        Route::post('meeting/confirm', 'ExternalController@confirmMeeting');
    });
});

Route::group(['middleware' => 'web'], function () {
    Route::auth();

    Route::get('/', 'HomeController@index');
    Route::get('/home', 'HomeController@index');
    Route::get('/live', 'HomeController@sortie');
    Route::resource('/tasks', 'TaskController');
    Route::resource('/entries', 'ReportController');
    Route::resource('/contacts', 'ContactController');
    Route::resource('/reporters', 'CoordinatorController');
    Route::resource('/announcements', 'AnnouncementController');
    Route::get('/reporters/{id}/area', 'CoordinatorController@areas');
    Route::post('/reporters/savearea', 'CoordinatorController@saveAreas');

    Route::get('/leaderboards', 'LeaderBoardController@index');


});
