<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Region;
use App\Models\Province;

class ImportProvince extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:province';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->comment('Start');
        $regions =  Region::all();

        foreach($regions as $region) {

            $this->getProvinceByRegion($region->RegionCode);
        }
        $this->info('Done');
    }

    protected function getProvinceByRegion($regionCode) {
        $url = 'http://prcws.appteknik.com/Service1.svc/getFormsProvince/' . $regionCode;
        $provinces = json_decode(file_get_contents($url), 1);

        foreach ($provinces['GetFormsProvinceResult'] as $province) {
            $province['RegionCode'] = $regionCode;
            Province::create($province)->save();
            $this->info($province['ProvinceName']);
        }

    }
}
