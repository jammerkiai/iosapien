<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Municipality;
use App\Models\Barangay;

class ImportBarangay extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:barangay';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->comment('Start');
        $muniCode = $this->ask('What MunicipalityCode? [ type "0" for all ]');

        if ($muniCode === '0') {
            $cities = Municipality::orderBy('MunicipalityCode')
                                    ->get();
        } else {
            $cities = Municipality::where('MunicipalityCode', '>=', $muniCode)
                                    ->orderBy('MunicipalityCode')
                                    ->get();
        }


        foreach($cities as $city) {
            $this->warn($city->RegionCode . ' ' . $city->ProvinceCode . ' ' . $city->MunicipalityCode);
            $this->getBarangays($city->MunicipalityCode, $city->ProvinceCode , $city->RegionCode);
        }

        $this->info("Done");
    }

    protected function getBarangays($municipalityCode, $provinceCode, $regionCode) {

        $url = 'http://prcws.appteknik.com/Service1.svc/getFormsBarangay/' . trim($municipalityCode);
        $barangays = json_decode(file_get_contents($url),1 );

        foreach ($barangays['GetFormsBarangayResult'] as $barangay) {
            $barangay['RegionCode'] = $regionCode;
            $barangay['ProvinceCode'] = $provinceCode;
            $barangay['MunicipalityCode'] = $municipalityCode;
            Barangay::create($barangay)->save();
            $this->comment($barangay['BarangayName']);

        }
    }
}
