<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Region;
use App\Models\Province;
use App\Models\Municipality;
use App\Models\Barangay;
use Schema;


class ImportRegions extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:regions';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'scrapes regions';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->comment('start');

        $regions = json_decode(file(config_path() . '/jsondata/regions.json')[0], 1);

        foreach($regions['GetFormsRegionResult']  as $region) {

            Region::create($region)->save();
            $this->comment($region['RegionName']);

        }

        $this->info('Done');

    }






}
