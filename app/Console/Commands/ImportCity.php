<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Province;
use App\Models\Municipality;

class ImportCity extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:city';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->comment('Start');
        $provinces = Province::all();

        foreach($provinces as $province) {
            $this->getMunicipalities($province->ProvinceCode, $province->RegionCode);
        }

        $this->info("Done");
    }

    protected function getMunicipalities($provinceCode, $regionCode) {
        $url = 'http://prcws.appteknik.com/Service1.svc/getFormsMunicipality/' . $provinceCode;
        $municipalities = json_decode(file_get_contents($url),1 );

        foreach ($municipalities['GetFormsMunicipalityResult'] as $municipality) {
            $municipality['RegionCode'] = $regionCode;
            $municipality['ProvinceCode'] = $provinceCode;
            Municipality::create($municipality)->save();
            $this->comment($municipality['MunicipalityName']);

        }
    }
}
