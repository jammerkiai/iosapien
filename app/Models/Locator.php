<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Locator extends Model
{
    protected $table = 'locator';
    protected $fillable = ['coordinatorId', 'longitude', 'latitude'];
    protected $hidden = ['created_at', 'updated_at'];

    public function coordinator() {
        return $this->belongsTo('App\Models\Coordinator', 'coordinatorId');
    }
}
