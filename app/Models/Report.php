<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Report extends Model
{
    protected $table = 'reports';
    protected $fillable = [
        'barangay', 'reportDate', 'streetAddress', 'task_id', 'coordinatorId',
        'activityRoute', 'feedback', 'photo', 'caption', 'latitude', 'longitude'
    ];

    protected $hidden = [
      'created_at', 'updated_at'
    ];

    public function coordinator() {
        return $this->belongsTo('App\Models\Coordinator', 'coordinatorId', 'id');
    }


    public function task() {
        return $this->belongsTo('App\Models\Task', 'task_id', 'id');
    }
}
