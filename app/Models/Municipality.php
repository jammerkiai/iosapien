<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Municipality extends Model
{
    protected $table = 'municipalities';
    protected $fillable = [
        'RegionCode', 'ProvinceCode',
        'MunicipalityName', 'MunicipalityCode',
        'MunicipalityLongitute', 'MunicipalityLatitude'
    ];

    public function region() {
        return $this->belongsTo('App\Models\Region', 'RegionCode', 'RegionCode');
    }

    public function province() {
        return $this->belongsTo('App\Models\Province', 'ProvinceCode', 'ProvinceCode');
    }

    public function coordinators() {
        return $this->belongsToMany('App\Models\Coordinator', 'coordinator_municipalities', 'MunicipalityCode', 'id');
    }
}
