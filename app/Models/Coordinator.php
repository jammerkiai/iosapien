<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Coordinator extends Model
{
    protected $table = 'coordinators';

    protected $fillable = [
        'firstname', 'lastname', 'middlename',
        'gender', 'birthday', 'mobile' , 'email',
        'password', 'active', 'position'
    ];

    protected $hidden = [
      'password'
    ];

    public function municipalities() {
        return CoordinatorMunicipality::where('id', [$this->id])->get(['MunicipalityCode']);
    }

    public function locator() {
        return $this->hasOne('App\Models\Locator', 'coordinatorId');
    }
}
