<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    protected $table = 'contacts';
    protected $fillable = [
        'firstname', 'lastname', 'middlename',
        'gender', 'birthday', 'mobile' , 'email',
        'remarks', 'coordinatorId'
    ];

    public function coordinator() {
        return $this->belongsTo('App\Models\Coordinator', 'coordinatorId', 'id');
    }
}
