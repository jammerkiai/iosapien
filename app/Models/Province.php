<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Province extends Model
{
    protected $table = 'provinces';
    protected $fillable = [ 'ProvinceName', 'ProvinceCode', 'RegionCode' ];

    public function region() {
        return $this->belongsTo('App\Models\Region', 'RegionCode', 'RegionCode');
    }

    public function municipalities() {
        return $this->hasMany('App\Models\Municipality', 'ProvinceCode', 'ProvinceCode');
    }
}
