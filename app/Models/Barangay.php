<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Barangay extends Model
{
    protected $table = 'barangays';
    protected $fillable = [
        "RegionCode", "ProvinceCode", "MunicipalityCode",
        "BarangayCode","BarangayLatitude","BarangayLongitude","BarangayName"
    ];

    public function municipality() {
        return $this->belongsTo('App\Models\Municipality', 'MunicipalityCode', 'MunicipalityCode');
    }

    public function region() {
        return $this->belongsTo('App\Models\Region', 'RegionCode', 'RegionCode');
    }

    public function province() {
        return $this->belongsTo('App\Models\Province', 'ProvinceCode', 'ProvinceCode');
    }
}
