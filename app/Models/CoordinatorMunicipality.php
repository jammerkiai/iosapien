<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CoordinatorMunicipality extends Model
{
    protected $table = 'coordinator_municipalities';
    public $timestamps = false;
}
