<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Region extends Model
{
    protected $table = 'regions';
    protected $fillable = [ 'RegionName', 'RegionCode' ];


    public function provinces() {
        return $this->hasMany('App\Models\Province', 'RegionCode', 'RegionCode');
    }

}
