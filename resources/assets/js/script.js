var advrollout = advrollout || {};
advrollout = (function () {

    var ancillary = {
        init: function () {
          if ($('#mapHolder').length > 0) { 
              google.maps.event.addDomListener(window, 'load', this.initialize);
          }else if ($('#mapSortie').length > 0) { 
              google.maps.event.addDomListener(window, 'load', this.initializeSortie);
          }else if ($('#mapEntry').length > 0) { 
            console.log('map details');
            google.maps.event.addDomListener(window, 'load', this.initializeEntryDetail);
          }
          

        },
        initialize: function () {

          var mapProp = {
              center: {lat: 14.58627, lng: 121.05732},
	            zoom: 10,
	            mapTypeId: google.maps.MapTypeId.ROADMAP,
	        };
	        var map = new google.maps.Map(document.getElementById("mapHolder"), mapProp);
          var markers = [];
          $.getJSON("https://advrollout.iosapien.com/api/map/reports", function(data) {
              $.each(data.reports, function(i, object) {
                  var image = '/images/person.png';
                  var latLng = new google.maps.LatLng(object.latitude, object.longitude);
                  if(markers.length != 0) {
                      for (i=0; i < markers.length; i++) {
                          var existingMarker = markers[i];
                          var pos = existingMarker.getPosition();
                          if (latLng.equals(pos)) {
                              var a = 360.0 / markers.length;
                              var newLat = parseFloat(object.latitude) + -.00010 * Math.cos((+a*i) / 180 * Math.PI);  //x
                              var newLng = parseFloat(object.longitude) + -.00010 * Math.sin((+a*i) / 180 * Math.PI);  //Y
                              var latLng = new google.maps.LatLng(newLat,newLng);
                          }
                      }
                  }
                  var marker = new google.maps.Marker({
                      position : latLng,
                      map: map,
                      animation: google.maps.Animation.DROP,
                      icon: image
                  });
                  var infowindow = new google.maps.InfoWindow({
                    content: '<div class="infoHolder"> \
                                <div class="infoImage"><img src="https://advrollout.iosapien.com/uploads/'+ object.photo +'" /></div> \
                                <div class="infoName">'+ object.task +'<br /></div> \
                                <div class="infoDescription"><div>'+ object.streetAddress +', '+ object.barangay +'</div><div><b>'+ object.coordinator +'</b></div></div> \
                              </div>',
                    maxWidth: 600
                  });
                  marker.addListener('click', function() {
                    infowindow.open(map, marker);
                    map.setCenter(marker.getPosition());
                  });
                  markers.push(marker);
              });
              var markerCluster = new MarkerClusterer(map, markers, { zoomOnClick: true, maxZoom: 15, gridSize: 20 });
          });
        },
        initializeEntryDetail: function() {
            
          loadEntryMap();
          setInterval(function () {
            loadEntryMap();
          }, 300000);
          function loadEntryMap(){
              var latitude = $('#mapEntry').data('lat');
              var longitude = $('#mapEntry').data('lng');
            var mapProp = {
                center: {lat: latitude, lng: longitude},
                zoom: 15,
                mapTypeId: google.maps.MapTypeId.ROADMAP,
            };
            var map = new google.maps.Map(document.getElementById("mapEntry"), mapProp);
            var image = '/images/person.png';
            var markers = [];
            var a = 360;
            var newLat = parseFloat(latitude) + -.00010 * Math.cos((+a) / 180 * Math.PI);  //x
            var newLng = parseFloat(longitude) + -.00010 * Math.sin((+a) / 180 * Math.PI);  //Y
            var latLng = new google.maps.LatLng(newLat,newLng);
            var marker = new google.maps.Marker({
                position : latLng,
                map: map,
                animation: google.maps.Animation.DROP,
                icon: image
            });
            markers.push(marker);
            var markerCluster = new MarkerClusterer(map, markers, { zoomOnClick: true, maxZoom: 15, gridSize: 20 });
          }
          
        },

        initializeSortie: function () {
          loadMap();
          setInterval(function () {
              loadMap();
          }, 60000);
          function loadMap(){
            var mapProp = {
                center: {lat: 14.58627, lng: 121.05732},
                zoom: 15,
                mapTypeId: google.maps.MapTypeId.ROADMAP,
            };
            var map = new google.maps.Map(document.getElementById("mapSortie"), mapProp);
            var markers = [];
            $.getJSON("https://advrollout.iosapien.com/api/map/locator", function(data) {

                $.each(data, function(i, object) {
                    var image = 'images/person.png';
                    var latLng = new google.maps.LatLng(object.latitude, object.longitude);

                    if(markers.length != 0) {
                        for (i=0; i < markers.length; i++) {
                            var existingMarker = markers[i];
                            var pos = existingMarker.getPosition();
                            if (latLng.equals(pos)) {
                                var a = 360.0 / markers.length;
                                var newLat = parseFloat(object.latitude) + -.00010 * Math.cos((+a*i) / 180 * Math.PI);  //x
                                var newLng = parseFloat(object.longitude) + -.00010 * Math.sin((+a*i) / 180 * Math.PI);  //Y
                                var latLng = new google.maps.LatLng(newLat,newLng);
                            }
                        }
                    }
                    var marker = new google.maps.Marker({
                        position : latLng,
                        map: map,
                        animation: google.maps.Animation.DROP,
                        icon: image
                    });
                    var infowindow = new google.maps.InfoWindow({
                      content: '<div class="infoHolder"> \
                                  <div class="infoName">'+ object.coordinatorName +'<br /></div> \
                                  <div class="infoDescription"><div style="margin-top:-15px;">'+ object.position +'<br/><br/><a href="tel:'+ object.mobile +'">'+ object.mobile +'</a></div></div> \
                                </div>',
                      maxWidth: 600
                    });
                    map.setCenter(marker.getPosition())
                    marker.addListener('click', function() {
                      infowindow.open(map, marker);
                      map.setCenter(marker.getPosition());
                    });
                    markers.push(marker);
                });
                var markerCluster = new MarkerClusterer(map, markers, { zoomOnClick: true, maxZoom: 15, gridSize: 20 });
            });
          }
          
        }
    };
    return {
        run: function () {
            ancillary.init();
        }
    };
}());

$(document).ready(function() {  
  advrollout.run();
});
