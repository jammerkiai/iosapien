@extends('layouts.app')
@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">Reporters
                        <a href="{{url('reporters/create')}}" class="pull-right">
                            Add New Reporter</a>
                    </div>
                    <div class="panel-body">
                        <table class="table table-responsive">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>FIRST NAME</th>
                                    <th>MIDDLE NAME</th>
                                    <th>LAST NAME</th>
                                    <th>EMAIL</th>
                                    <th>MOBILE</th>
                                    <th>BIRTHDAY</th>
                                    <th>GENDER</th>
                                    <th>POSITION</th>
                                    <th>ACTIVE</th>
                                    <th>ACTION</th>
                                </tr>
                            </thead>
                            <tbody>
                            @forelse($coordinators as $coordinator)
                                <tr>
                                    <td>{{ $coordinator->id }}</td>
                                    <td>{{ $coordinator->firstname }}</td>
                                    <td>{{ $coordinator->middlename }}</td>
                                    <td>{{ $coordinator->lastname }}</td>
                                    <td>{{ $coordinator->email }}</td>
                                    <td>{{ $coordinator->mobile }}</td>
                                    <td>{{ $coordinator->birthday }}</td>
                                    <td>{{ $coordinator->gender }}</td>
                                    <td>{{ $coordinator->position }}</td>
                                    <td>
                                        <span class="glyphicon @if($coordinator->active==1) glyphicon-ok @else glyphicon-remove @endif"></span>
                                    </td>
                                    <td>

                                        <a class="btn btn-sm btn-primary" href="{{ url('reporters', $coordinator->id) }}/area">Areas</a>
                                        <a class="btn btn-sm btn-info" href="{{ url('reporters', $coordinator->id) }}/edit">Edit</a>

                                        <form style="display:inline-block" class="form form-inline" method="POST" action="{{ url('reporters', $coordinator->id) }}">
                                            <button type="submit" class="btn btn-sm btn-danger">Delete</button>
                                            <input type="hidden" name="_method" value="DELETE" />
                                            {!! csrf_field() !!}
                                        </form>

                                    </td>
                                </tr>
                            @empty
                                <tr><td colspan="11">No reporters</td></tr>
                            @endforelse
                            </tbody>
                            <tfoot>
                            <tr><td colspan="11"><center>{!! $coordinators->links() !!}</center></td></tr>
                            </tfoot>
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection
