@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Provinces Covered by:
                        <strong>{{$coordinator->lastname}}, {{$coordinator->firstname}}</strong>

                        <a href="{{url('reporters') }}" class="pull-right">Back to List</a>
                    </div>
                    <input type="hidden" id="coord_id" value="{{$coordinator->id}}" />

                    <div class="panel-body">
                        <ol>
                            @foreach($regions as $region)
                                <li>
                                    <input type="checkbox" name="region" value="{{$region->RegionCode}}" class="region" />
                                    {{$region->RegionName}}
                                    <ol>
                                        @foreach($region->provinces as $province)
                                            <li>
                                                <input type="checkbox" name="province" value="{{ $province->ProvinceCode}}"
                                                       class="province rg{{$region->RegionCode}}"
                                                            @foreach($province->municipalities as $muni)
                                                                @if (in_array($muni->MunicipalityCode, $cities) )
                                                                    checked
                                                                @endif
                                                            @endforeach
                                                        />
                                                {{$province->ProvinceName}}
                                               @if($region->RegionCode === '130000000')
                                                <ol>
                                                    @foreach($province->municipalities as $municipality)
                                                        <li>
                                                            <input type="checkbox" name="municipality"
                                                                   value="{{$municipality->MunicipalityCode}}"
                                                                   class="municipality pr{{$province->ProvinceCode}}"
                                                                    @if(in_array($municipality->MunicipalityCode, $cities)) checked @endif
                                                                    />

                                                            {{$municipality->MunicipalityName}}
                                                        </li>
                                                    @endforeach
                                                </ol>
                                                @endif
                                            </li>
                                        @endforeach
                                    </ol>
                                </li>
                            @endforeach
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
        @endsection
        @section('customjs')
            <script>

                $(document).ready(function(){
                    $('.municipality').on('click, change', function(){
                        var ischecked = $(this).is(':checked');
                        var munCode = $(this).val();
                        var coordId = $('#coord_id').val();
                        var url = "{{url('reporters/savearea')}}";
                        $.post(url,
                            {
                              "id" : coordId, "MunicipalityCode": munCode, "checked" : ischecked
                            },
                            function(resp){
                                console.log(resp);
                            }
                        );
                    });

                    $('.province').on('click', function(e){
                        var tgtClass = '.pr' + $(this).val();

                        if ($(this).hasClass('rg130000000')) {
                            $(tgtClass).trigger('click');
                        } else {

                            var ischecked = $(this).is(':checked');
                            var ProvinceCode = $(this).val();
                            var coordId = $('#coord_id').val();
                            var url = "{{url('reporters/savearea')}}";

                            $.post(url,
                                {
                                    "id" : coordId, "ProvinceCode": ProvinceCode, "checked" : ischecked
                                },
                                function(resp){
                                    console.log(resp);
                                }
                            );

                        }
                    });

                    $('.region').on('click', function(e){
                        var tgtClass = '.rg' + $(this).val();
                        $(tgtClass).trigger('click');
                    })
                });

            </script>
@endsection
