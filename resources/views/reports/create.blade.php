@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Add New Entry</div>
                    <div class="panel-body">
                        <form class="form-horizontal" role="form"
                              method="POST" action="{{ url('entries') }}" enctype="multipart/form-data">
                            {!! csrf_field() !!}

                            <div class="form-group{{ $errors->has('reportDate') ? ' has-error' : '' }}">
                                <label class="col-md-4 control-label">Date</label>

                                <div class="col-md-6">
                                    <input type="date" class="form-control"  id="reportDate"
                                           name="reportDate" value="{!! date('Y-m-d') !!}">

                                    @if ($errors->has('reportDate'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('reportDate') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('coordinatorId') ? ' has-error' : '' }}">
                                <label class="col-md-4 control-label">Reporter</label>
                                <div class="col-md-6">
                                    <select class="form-control" name="coordinatorId">
                                        <option value="">Select reporter</option>
                                        @foreach(\App\Models\Coordinator::all() as $coordinator)
                                            <option value="{{ $coordinator->id }}">
                                                {{ $coordinator->firstname }} {{ $coordinator->lastname }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('task_id') ? ' has-error' : '' }}">
                                <label class="col-md-4 control-label">Task</label>
                                <div class="col-md-6">
                                    <select class="form-control" name="task_id">
                                        <option value="">Select Task</option>
                                        @foreach(\App\Models\Task::all() as $task)
                                            <option value="{{ $task->id }}">
                                                {{ $task->name }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('barangay') ? ' has-error' : '' }}">
                                <label class="col-md-4 control-label">Barangay</label>
                                <div class="col-md-6">
                                    <input type="text" class="form-control"  id="barangay"
                                           name="barangay" value="{{ old('barangay') }}">

                                    @if ($errors->has('barangay'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('barangay') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('streetAddress') ? ' has-error' : '' }}">
                                <label class="col-md-4 control-label">Street Address</label>

                                <div class="col-md-6">
                                    <input type="text" class="form-control"  id="streetAddress"
                                           name="streetAddress" value="{{ old('streetAddress') }}">

                                    @if ($errors->has('streetAddress'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('streetAddress') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('activityRoute') ? ' has-error' : '' }}">
                                <label class="col-md-4 control-label">Proposed Activity / Route</label>

                                <div class="col-md-6">
                                    <textarea class="form-control"  id="activityRoute" name="activityRoute">
                                    </textarea>

                                    @if ($errors->has('activityRoute'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('activityRoute') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('feedback') ? ' has-error' : '' }}">
                                <label class="col-md-4 control-label">Actual Accomplishment / Report / Feedback</label>

                                <div class="col-md-6">
                                    <textarea class="form-control"  id="feedback" name="feedback">
                                    </textarea>

                                    @if ($errors->has('feedback'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('feedback') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('caption') ? ' has-error' : '' }}">
                                <label class="col-md-4 control-label">Photo Caption</label>

                                <div class="col-md-6">
                                    <input type="text" class="form-control"  id="caption"
                                           name="caption" value="{{ old('caption') }}">

                                    @if ($errors->has('caption'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('caption') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('photo') ? ' has-error' : '' }}">
                                <label class="col-md-4 control-label">Upload Photo</label>

                                <div class="col-md-6">
                                    <input type="file" class="form-control"  id="photo"
                                           name="photo" value="{{ old('photo') }}">

                                    @if ($errors->has('photo'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('photo') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('latitude') ? ' has-error' : '' }}">
                                <label class="col-md-4 control-label">latitude</label>

                                <div class="col-md-6">
                                    <input type="text" class="form-control"  id="latitude"
                                           name="latitude" value="{{ old('latitude') }}">

                                    @if ($errors->has('latitude'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('latitude') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('longitude') ? ' has-error' : '' }}">
                                <label class="col-md-4 control-label">longitude</label>

                                <div class="col-md-6">
                                    <input type="text" class="form-control"  id="longitude"
                                           name="longitude" value="{{ old('longitude') }}">

                                    @if ($errors->has('longitude'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('streetAddress') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>




                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-4">
                                    <a href="{{  url('entries') }}" class="btn btn-warning">
                                        <i class="fa fa-btn fa-arrow-left"></i>Back to List
                                    </a>
                                    <button type="submit" class="btn btn-primary">
                                        <i class="fa fa-btn fa-save"></i>Submit
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('customjs')
    <script>

        $(document).ready(function(){
            $('#name').trigger('focus');
        });

    </script>
@endsection
