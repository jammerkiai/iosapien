@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Details</div>
                    <div class="panel-body">
                        <div  id="mapEntry" data-lat="{{ $report->latitude or '14.58627' }}" 
                        data-lng="{{ $report->longitude  or '121.05732' }}"></div>
                    </div>
                    <div class="panel-body">
                    
                       <table class="table table-responsive">
                            <tr>
                                <td>Date Reported</td>
                                <th>{{ $report->reportDate  }}</th>
                            </tr>
                           <tr>
                               <td>Reported By</td>
                               <th>{!! App\Models\Coordinator::find($report->coordinatorId)['firstname'] !!}
                               {!! App\Models\Coordinator::find($report->coordinatorId)['lastname'] !!}</th>
                           </tr>
                           <tr>
                               <td>Location</td>
                               <th>
                                   {{ $report->barangay or ''}}

                               </th>
                           </tr>
                           <tr>
                               <td>Coords</td>
                               <th>
                                   {{ $report->latitude or ''}}, {{ $report->longitude or ''}}
                               </th>
                           </tr>
                           <tr>
                               <td>Street Address</td>
                               <th>{{ $report->streetAddress }}</th>
                           </tr>

                           <tr>
                               <td>Task</td>
                               <th>{!! App\Models\Task::find($report->task_id)['name'] !!}</th>
                           </tr>
                           <tr>
                               <td>Proposed Activity / Route</td>
                               <th>{{ $report->activityRoute }}</th>
                           </tr>
                           <tr>
                               <td>Actual Accomplishment / Report / Feedback</td>
                               <th>{{ $report->feedback }}</th>
                           </tr>
                           <tr>
                               <th colspan="2">
                                   <h4>{{$report->caption}}</h4>
                                    <img src="{{ asset('uploads') . '/' .$report->photo }}" width="600px" />
                               </th>
                           </tr>
                       </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
