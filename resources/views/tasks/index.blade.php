@extends('layouts.app')
@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Tasks
                        <a href="{{url('tasks/create')}}" class="pull-right">
                            Add New Task</a>
                    </div>
                    <div class="panel-body">
                        <table class="table ">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>NAME</th>
                                    <th>ACTIVE</th>
                                    <th>ACTION</th>
                                </tr>
                            </thead>
                            <tbody>
                            @forelse($tasks as $task)
                                <tr>
                                    <td>{{ $task->id }}</td>
                                    <td>{{ $task->name }}</td>
                                    <td>
                                        <span class="glyphicon @if($task->active==1) glyphicon-ok @else glyphicon-remove @endif"></span>
                                    </td>
                                    <td>

                                        <a class="btn btn-sm btn-info" href="{{ url('tasks', $task->id) }}/edit">Edit</a>

                                        <form style="display:inline-block" class="form form-inline" method="POST" action="{{ url('tasks', $task->id) }}">
                                            <button type="submit" class="btn btn-sm btn-danger">Delete</button>
                                            <input type="hidden" name="_method" value="DELETE" />
                                            {!! csrf_field() !!}
                                        </form>

                                    </td>
                                </tr>
                            @empty
                                <tr><td colspan="4">No tasks</td></tr>
                            @endforelse
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection
