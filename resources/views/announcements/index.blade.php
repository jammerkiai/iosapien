@extends('layouts.app')
@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                @if(Session::has('message'))
                    <div class="alert alert-dismissable alert-info">{{Session::get('message')}}</div>
                @endif
                <div class="panel panel-default">
                    <div class="panel-heading">Announcements
                        <a href="{{url('announcements/create')}}" class="pull-right">
                            Add New announcement</a>
                    </div>

                    <div class="panel-body">
                        <table class="table table-responsive">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Date</th>
                                <th>Announcement</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @forelse($announcements as $announcement)
                                <tr>
                                    <td>{{ $announcement->id }}</td>
                                    <td>{{ $announcement->created_at }}</td>
                                    <td>
                                        <strong>{{ $announcement->title }}</strong>
                                        <p>{{ $announcement->body }}</p>
                                    </td>
                                    <td>
                                        <a class="btn btn-sm btn-info" href="{{ url('announcements', $announcement->id) }}/edit">Edit</a>

                                        <form style="display:inline-block" class="form form-inline" method="POST" action="{{ url('announcements', $announcement->id) }}">
                                            <button type="submit" class="btn btn-sm btn-danger">Delete</button>
                                            <input type="hidden" name="_method" value="DELETE" />
                                            {!! csrf_field() !!}
                                        </form>

                                    </td>
                                </tr>
                            @empty
                                <tr><td colspan="11">No announcements</td></tr>
                            @endforelse
                            </tbody>
                            <tfoot>
                            <tr><td colspan="11"><center>{!! $announcements->links() !!}</center></td></tr>
                            </tfoot>
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection
