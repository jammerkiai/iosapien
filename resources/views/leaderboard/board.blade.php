<table class="table table-responsive">
    <thead>
    <tr>
        <th>RANK</th>
        <th>REPORTER</th>
        <th>TOTAL APP SUBMISSIONS</th>
        <th>NO. OF ACTIVITY REPORTS</th>
        <th>NO. OF CONTACTS</th>
    </tr>
    </thead>
    <tbody>
    @forelse($results as $key => $res)
        <tr>
            <td>{{ $key + 1 }}</td>
            <td>{{ $res['coordinatorname'] }}</td>
            <td>
                {{ $res['total'] }}
            </td>
            <td>
                {{ $res['reports']}}
            </td>
            <td>
                {{ $res['contacts'] }}
            </td>
        </tr>
    @empty
        <tr><td colspan="8">No results</td></tr>
    @endforelse
    </tbody>
</table>
