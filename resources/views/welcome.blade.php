@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Welcome</div>

                <div class="panel-body">
                    <img src="{{ asset('images/advrollout_logo.png') }}" />
                    Login Required.
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
