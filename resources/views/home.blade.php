@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>

                <div class="panel-body">

                    <div class="btn-group">
                        <button class="btn btn-success btn-lg dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Leader Boards <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu">
                            <li><a href="{{ url('/leaderboards')  }}">View</a></li>
                        </ul>
                    </div>

                    <div class="btn-group">
                        <button class="btn btn-primary btn-lg dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Reporters <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu">
                            <li><a href="{{ url('/reporters/create')  }}">Add New</a></li>
                            <li><a href="{{ url('/reporters')  }}">View List</a></li>
                        </ul>
                    </div>

                    <div class="btn-group">
                        <button class="btn btn-warning btn-lg dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Tasks <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu">
                            <li><a href="{{ url('/tasks/create')  }}">Add New</a></li>
                            <li><a href="{{ url('/tasks')  }}">View List</a></li>
                        </ul>
                    </div>

                    

                    <div class="btn-group">
                        <button class="btn btn-info btn-lg dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Entries <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu">
                            <li><a href="{{ url('/entries')  }}">View</a></li>
                        </ul>
                    </div>

                    <div class="btn-group">
                        <button class="btn btn-danger btn-lg dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Contacts <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu">
                            <li><a href="{{ url('/contacts')  }}">View</a></li>
                        </ul>
                    </div>

                    <div class="btn-group">
                        <button class="btn btn-normal btn-lg dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Announcements <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu">
                            <li><a href="{{ url('/announcements')  }}">View</a></li>
                        </ul>
                    </div>

                    <div  id="mapHolder"></div>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
