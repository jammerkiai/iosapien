@extends('layouts.app')
@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">Contacts

                    </div>
                    <div class="panel-body">
                        <table class="table table-responsive">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>FIRST NAME</th>
                                <th>MIDDLE NAME</th>
                                <th>LAST NAME</th>
                                <th>GENDER</th>
                                <th>BIRTHDAY</th>
                                <th>MOBILE</th>
                                <th>EMAIL</th>
                                <th>ENDORSED BY</th>
                                {{--<th>ACTION</th>--}}
                            </tr>
                            </thead>
                            <tbody>
                            @forelse($contacts as $contact)
                                <tr>
                                    <td>{{ $contact->id }}
                                    <td>{{ $contact->firstname }}</td>
                                    <td>{{ $contact->middlename }}</td>
                                    <td>{{ $contact->lastname }}</td>

                                    <td>{{ $contact->gender }}</td>
                                    <td>{{ $contact->birthday }}</td>
                                    <td>{{ $contact->mobile }}</td>
                                    <td>{{ $contact->email }}</td>
                                    <td>{{ $contact->coordinator->firstname or ''}} {{ $contact->coordinator->lastname or ''}}</td>
                                    {{--<td>--}}
                                        {{--<a class="btn btn-sm btn-primary" href="{{ url('contacts', $contact->id) }}">View</a>--}}
                                    {{--</td>--}}
                                </tr>
                            @empty
                                <tr><td colspan="10">No contacts</td></tr>
                            @endforelse
                            </tbody>
                            <tfoot>
                            <tr><td colspan="10"><center>{!! $contacts->links() !!}</center></td></tr>
                            </tfoot>
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection
