var gulp = require('gulp'),
	uglify = require('gulp-uglify'),
	sass = require('gulp-ruby-sass');


/*
 |--------------------------------------------------------------------------
 | JS taskrunner
 |--------------------------------------------------------------------------
 */

 gulp.task('script', function() {
  return gulp.src('resources/assets//js/*.js')
    .pipe(gulp.dest('public/js/'));
});

/*
 |--------------------------------------------------------------------------
 | CSS taskrunner
 |--------------------------------------------------------------------------
 */
gulp.task('sass', function () {
	return sass('resources/assets/sass/*.scss')
    	.pipe(gulp.dest('public/css'));
});

/*
 |--------------------------------------------------------------------------
 | Watch taskrunner
 |--------------------------------------------------------------------------
 */

gulp.task('watch', function () {
	gulp.watch('resources/assets/sass/*.scss', ['sass']);
	gulp.watch('resources/assets//js/*.js', ['script']);
});

gulp.task('default', ['script', 'sass', 'watch']);